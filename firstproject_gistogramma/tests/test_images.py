import PyQt5
from PyQt5 import QtGui
from image_editor import image_functions
from matplotlib import pyplot as plt
import qimage2ndarray
import numpy as np

def test_all_white():
    img = image_functions.load_image(
        "all-white100x100.bmp")
    hist = image_functions.hist(img)
    assert len(hist) == 3
    for color_hist in hist:
        plt.plot(color_hist)
        assert len(color_hist) == 256
        assert sum(color_hist) == 10000
        assert color_hist[ 255] == 10000
        for el in color_hist[:-1]:
            assert el == 0

def test_all_black():
    img = image_functions.load_image(
        "all-black100x100.bmp")
    hist = image_functions.hist(img)
    assert len(hist) == 3
    for color_hist in hist:
        assert len(color_hist) == 256
        assert sum(color_hist) == 10000
        assert color_hist[0] == 10000
        for el in color_hist[1:]:
            assert el == 0

def test_all_red():
    img = image_functions.load_image(
        "all-red100x100.bmp")
    hist = image_functions.hist(img)
    assert len(hist) == 3
    for color_hist in hist:
        assert len(color_hist) == 256
        assert sum(color_hist) == 10000
        for el in color_hist[1:-1]:
            assert el == 0
    assert hist[0][0] == 0
    assert hist[0][255] == 10000
    assert hist[1][0] == 10000
    assert hist[1][255] == 0
    assert hist[2][0] == 10000
    assert hist[2][255] == 0

def test_halftone():
    img = image_functions.load_image(
        "all-red100x100.bmp")
    ht_img = image_functions.make_halftone(img)

    assert ht_img.isGrayscale() == True

def test_diff():
    img1 = image_functions.load_image(
        "all-red100x100.bmp")
    img2 = image_functions.load_image(
        "all-red100x100.bmp")
    
    img3 = image_functions.make_diff(img1, img2)

    assert img3.pixelColor(50,50) == QtGui.QColor(127,127,127)

def test_add():
    img1 = image_functions.load_image("all-gray100x100.png")
    img2 = image_functions.load_image("all-black100x100.bmp")
    img3 = image_functions.make_sum(img1, img2)
    assert img3 == img1

def test_mult():
    img1 = image_functions.load_image("all-gray100x100.png")
    img2 = image_functions.load_image("all-black100x100.bmp")
    img3 = image_functions.make_mult(img1, 0)
    assert img3 == img2

def test_binarization():
    img1 = image_functions.load_image("all-gray100x100.png")
    img2 = image_functions.make_bin(img1, 128)
    img2 = qimage2ndarray.rgb_view(img2)

    assert not np.any(img2)

def test_prewitt():
    img1 = image_functions.load_image("borders_example.jpg")
    img1 = image_functions.make_halftone(img1)
    img2 = image_functions.make_prewitt(img1)
    
    assert img2.pixelColor(50, 0).lightness() == 255
    assert img2.pixelColor(50, 1).lightness() == 255
    assert img2.pixelColor(50, 2).lightness() == 0

def test_sobel():
    img1 = image_functions.load_image("borders_example.jpg")
    img1 = image_functions.make_halftone(img1)
    img2 = image_functions.make_sobel(img1)

    assert img2.pixelColor(50, 0).lightness() == 255
    assert img2.pixelColor(50, 1).lightness() == 255
    assert img2.pixelColor(50, 2).lightness() == 0

def test_laplas():
    img1 = image_functions.load_image("borders_example.jpg")
    img1 = image_functions.make_halftone(img1)
    img2 = image_functions.make_laplas(img1)

    a = img2.pixelColor(50, 0).lightness()
    b = img2.pixelColor(50, 1).lightness()
    c = img2.pixelColor(50, 2).lightness()

    a+=127;b+=127;c+=127
    a = 255 if a > 255 else a
    b = 255 if b > 255 else b
    c = 255 if c > 255 else c

    assert a == 255
    assert b == 255
    assert c == 128

def test_move():
    img1 = image_functions.load_image("all-white100x100.bmp")
    img2 = image_functions.make_move(img1,50,50)
    
    assert img2.pixelColor(1,1) == QtGui.QColor("black")
    assert img2.pixelColor(50,50) == QtGui.QColor("black")