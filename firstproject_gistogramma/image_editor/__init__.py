# This Python file uses the following encoding: utf-8
import sys, os

import PyQt5
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QImage, QPixmap
from PyQt5.QtWidgets import QApplication
from .ImageViewer import ImageViewer


def main():
    app = QApplication.instance()
    if app is None:
        app = QApplication([])

    widget = ImageViewer()
    widget.show()
    app.exec_()
